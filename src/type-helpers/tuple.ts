const tuple = <T extends any[]>(...data: T) => data;

export default tuple;
