export { default as unary } from './functions/unary';
export { default as identity } from './functions/identity';
export { default as constant } from './functions/constant';
export { default as spreadArgs } from './functions/spread-args';
