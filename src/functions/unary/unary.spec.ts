import { expect } from 'chai';

import unary from './unary';

const sum = (addend1 = 0, addend2 = 0, addend3 = 0) => addend1 + addend2 + addend3;

describe('Unary function', () => {
  it('should wrap function and return function that is insensitive for rest of passed parameters except first', () => {
    const value = 15;
    const unarySum = unary(sum);
    expect(unarySum(value, 10, 20)).to.equal(value);
  });
});
