/* eslint-disable @typescript-eslint/no-unused-vars */
function unary<T, U>(fn: (arg: T, ...rest: any[]) => U) {
  return (arg: T, ...rest: any[]) => fn(arg);
}

export default unary;
