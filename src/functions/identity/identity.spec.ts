import { expect } from 'chai';

import identity from './identity';

describe('Identity function', () => {
  it('should wrap value and return that value', () => {
    const value = 'Value';
    const identityValue = identity(value);
    expect(identityValue).to.equal(value);
  });
});
