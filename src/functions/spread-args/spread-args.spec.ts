import { expect } from 'chai';

import spreadArgs from './spread-args';
import { tuple } from '../../type-helpers';

const sum = (addend1 = 0, addend2 = 0, addend3 = 0) => addend1 + addend2 + addend3;

describe('Identity function', () => {
  it('should wrap function that has named parameter and return function that will receive array istead of named parameters', () => {
    const value = tuple(10, 20);
    const spreadFunction = spreadArgs(sum);
    expect(spreadFunction).to.be.a('function');
    expect(spreadFunction(value)).to.equal(value.reduce((sum, addend) => sum + addend, 0));
  });
});
