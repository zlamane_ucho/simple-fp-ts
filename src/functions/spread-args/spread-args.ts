const spreadArgs = <T extends Array<any>, U>(fn: (...args: Readonly<T>) => U) => (args: T) => fn(...args);

export default spreadArgs;
