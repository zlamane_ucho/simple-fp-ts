import { expect } from 'chai';

import constant from './constant';

describe('Constant function', () => {
  it('should wrap value and return function that returns wrapped value', () => {
    const value = 'Value';
    const lazyValue = constant(value);
    const returnedValue = lazyValue();
    expect(lazyValue).to.be.a('function');
    expect(returnedValue).to.equal(value);
  });
});
